package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
class ErrorHandleControllerTest {
    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void should_catch_runtime_error() {
        ResponseEntity<ErrorResponse> entity = testRestTemplate.getForEntity("/api/errors/runtime-exception", ErrorResponse.class);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, entity.getStatusCode());
        assertEquals("run time exception", entity.getBody().getErrMsg());

    }

    @Test
    void should_catch_access_error() {
        ResponseEntity<ErrorResponse> entity = testRestTemplate.getForEntity("/api/exceptions/access-control-exception", ErrorResponse.class);
        assertEquals("access control exception", entity.getBody().getErrMsg());
    }

    @Test
    void should_match_access_error() {
        ResponseEntity<ErrorResponse> entity = testRestTemplate.getForEntity("/api/exceptions/access-control-exception", ErrorResponse.class);
        assertEquals("access control exception", entity.getBody().getErrMsg());
        assertEquals(HttpStatus.FORBIDDEN, entity.getStatusCode());
    }

    //2.1
    @Test
    void should_throw_default_error() {
        ResponseEntity<Void> entity = testRestTemplate.getForEntity("/api/errors/default", Void.class);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, entity.getStatusCode());
    }

    //2.2
    @Test
    void should_handle_illegal_argument_error() {
        ResponseEntity<ErrorResponse> entity = testRestTemplate.getForEntity("/api/errors/illegal-argument", ErrorResponse.class);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, entity.getStatusCode());

    }

    //2.3
    @Test
    void should_handle_multiple_exception() {
        ResponseEntity<ErrorResponse> entity = testRestTemplate.getForEntity("/api/errors/null-pointer", ErrorResponse.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, entity.getStatusCode());
        assertEquals("Something wrong with the argument", entity.getBody().getErrMsg());
    }

    @Test
    void should_handle_multiple_exception_another_exception() {
        ResponseEntity<ErrorResponse> entity = testRestTemplate.getForEntity("/api/errors/arithmetic", ErrorResponse.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, entity.getStatusCode());
        assertEquals("Something wrong with the argument", entity.getBody().getErrMsg());
    }

    //2.4
    @Test
    void should_handle_sis_with_controller_advice() {
        ResponseEntity<ErrorResponse> entity = testRestTemplate.getForEntity("/api/sister-errors/illegal-argument", ErrorResponse.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, entity.getStatusCode());
        assertEquals("Something wrong with brother or sister.",
                entity.getBody().getErrMsg());
        assertEquals(MediaType.APPLICATION_JSON, entity.getHeaders().getContentType());
    }

    @Test
    void should_handle_brother_with_controller_advice() {
        ResponseEntity<ErrorResponse> entity = testRestTemplate.getForEntity("/api/brother-errors/illegal-argument", ErrorResponse.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT, entity.getStatusCode());
        assertEquals("Something wrong with brother or sister.",
                entity.getBody().getErrMsg());
        assertEquals(MediaType.APPLICATION_JSON, entity.getHeaders().getContentType());
    }
}

package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UseControllerAdviceBrother {
    @GetMapping("/api/brother-errors/illegal-argument")
    public void responseUseControllerAdviceBrother() {
        throw new IllegalArgumentException("Something wrong with brother or sister.");
    }
}

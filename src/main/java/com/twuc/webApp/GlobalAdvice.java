package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalAdvice {
    @ExceptionHandler(value = {IllegalArgumentException.class})
    protected ResponseEntity handleIllegalException(RuntimeException exception) {
        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT)
                .contentType(MediaType.APPLICATION_JSON)
                .body(new ErrorResponse("418", exception.getMessage()));
    }
}

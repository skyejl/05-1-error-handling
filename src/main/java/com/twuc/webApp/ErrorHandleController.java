package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.AccessControlException;

@RestController

public class ErrorHandleController {
    //2.1
    @GetMapping("/api/errors/default")
    public void responseHandleDefault() {
        throw new RuntimeException("run time exception default");
    }
    @GetMapping("/api/errors/runtime-exception")
    public void responseHandle() {
        throw new RuntimeException("run time exception");
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ErrorResponse> responseErrorHandle(RuntimeException exception) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ErrorResponse("500", exception.getMessage()));
    }

    @GetMapping("/api/exceptions/access-control-exception")
    public void responseHandleAccessError() {
        throw new AccessControlException("access control exception");
    }

    @ExceptionHandler(AccessControlException.class)
    public ResponseEntity<ErrorResponse> responseErrorHandle(AccessControlException exception) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN)
                .body(new ErrorResponse("403", exception.getMessage()));
    }
    //2.2
    @GetMapping("/api/errors/illegal-argument")
    public void responseHandleIllegalError() {
        throw new IllegalArgumentException("Something wrong with the argument");
    }
    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ErrorResponse> handleIllegalException(IllegalArgumentException exception) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ErrorResponse("500", exception.getMessage()));
    }
    //2.3
    @GetMapping("/api/errors/null-pointer")
    public void responseWithNullPointerError() {
        throw new NullPointerException("Something wrong with the argument");
    }
    @GetMapping("api/errors/arithmetic")
    public void responseWithArithmeticError() {
        throw new ArithmeticException("Something wrong with the argument");
    }
    @ExceptionHandler({NullPointerException.class, ArithmeticException.class})
    public ResponseEntity<ErrorResponse> handleMultipleException(Exception exception) {
        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT)
                .body(new ErrorResponse("418", exception.getMessage()));
    }
}

package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UseControllerAdviceSis {
    @GetMapping("/api/sister-errors/illegal-argument")
    public void responseUseControllerAdviceSis() {
        throw new IllegalArgumentException("Something wrong with brother or sister.");
    }
}

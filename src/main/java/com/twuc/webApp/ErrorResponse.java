package com.twuc.webApp;

public class ErrorResponse {
    private String errorCode;
    private String errMsg;
    public ErrorResponse(String errorCode, String errMsg) {
        this.errMsg = errMsg;
        this.errorCode = errorCode;
    }
    public String getErrorCode() {
        return errorCode;
    }

    public String getErrMsg() {
        return errMsg;
    }
}
